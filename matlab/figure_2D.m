%% init

str = [];

while isempty(str)
prompt = 'Please type or copy the path to your obob_ownft:';
str = input(prompt, 's');
end

addpath(str);
cfg = [];
cfg.package.svs = true;
obob_init_ft(cfg);

if ispc
    
    addpath('.\utility_functions\');
    
    % define in dirs
    in_dir_stats = '.\data\meg_stats\';
    
    % load atlas
    atlas = ft_read_atlas([str '\external\fieldtrip\template\atlas\aal\ROI_MNI_V4.nii']);
    
else
    
    addpath('./utility_functions/');
    
    % define in dirs
    in_dir_stats = './data/meg_stats/';
    
    % load atlas
    atlas = ft_read_atlas([str '/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii']);
    
end

% load grid and convert to m
load mni_grid_1_cm_2982pnts
template_grid = ft_convert_units(template_grid,'m');

% convert atlas to m
atlas = ft_convert_units(atlas,'m');

% load mri
load standard_mri_better

font_name = 'Helvetica-Normal';

%% load data

load([in_dir_stats, 'regfac5_cluster_3-25hz_avgovertime.mat']);

%% prepare alpha stat plot

% virtual sens to source
cfg = [];
cfg.sourcegrid = template_grid;
cfg.parameter = 'stat';
cfg.mri = mri;
cfg.latency = [-1.75 -0.05];
cfg.frequency = [10 11];
alpha_source_stat = obob_svs_virtualsens2source(cfg, cntrst_stat);

cfg.parameter = 'mask'; % posclusterslabelmat
source_mask = obob_svs_virtualsens2source(cfg, cntrst_stat);

alpha_source_stat.mask = source_mask.mask;

max_alpha_source_stat = round(max(max(max(alpha_source_stat.stat))));
min_abs_alpha_source_stat = round(abs(min(min(min(alpha_source_stat.stat)))));

if max_alpha_source_stat > min_abs_alpha_source_stat
  
  color_lim_alpha_stat = max_alpha_source_stat;
  
else
  
  color_lim_alpha_stat = min_abs_alpha_source_stat;
  
end

%% prepare beta stat plot

% virtual sens to source
cfg = [];
cfg.sourcegrid = template_grid;
cfg.parameter = 'stat';
cfg.mri = mri;
cfg.latency = [-1.75 -0.05];
cfg.frequency = [16 18];
beta_source_stat = obob_svs_virtualsens2source(cfg, cntrst_stat);

cfg.parameter = 'mask'; % posclusterslabelmat
source_mask = obob_svs_virtualsens2source(cfg, cntrst_stat);

beta_source_stat.mask = source_mask.mask;

max_beta_source_stat = round(max(max(max(beta_source_stat.stat))));
min_abs_beta_source_stat = round(abs(min(min(min(beta_source_stat.stat)))));

if max_beta_source_stat > min_abs_beta_source_stat
  
  color_lim_beta_stat = max_beta_source_stat;
  
else
  
  color_lim_beta_stat = min_abs_beta_source_stat;
  
end

%% set global color lim

if color_lim_alpha_stat > color_lim_beta_stat
    
    color_lim_stat = color_lim_alpha_stat;
    
else
    
    color_lim_stat = color_lim_beta_stat;
    
end

%--------------------------------------------------------------------------
%%                       do alpha stat plot
%--------------------------------------------------------------------------
%% plot alpha stat left, medial, bottom

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_stat color_lim_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, alpha_source_stat)

colorbar('off');

% h = colorbar();
% h.Ticks = [-color_lim_stat 0 color_lim_stat];
% h.TickLength = 0;
% h.FontName = font_name;
% h.FontSize = 10;
% h.Title.String = 'T';
% h.Title.FontAngle = 'italic';
% h.Title.FontSize = 10;
% h.Title.FontName = font_name;
% h.Box = 'off';

view(90, -45) % left, medial, bottom
material dull
camlight headlight

%% plot alpha stat left, lateral, back

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_stat color_lim_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, alpha_source_stat)

colorbar('off');

view(-45, 0) % left, lateral, back
material dull
camlight headlight

%% plot alpha stat right, medial, bottom

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_stat color_lim_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_right.mat';
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, alpha_source_stat)

colorbar('off');

view(-90, -45) % right, medial, bottom
material dull
camlight headlight

%% plot alpha stat right, lateral, back

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_stat color_lim_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_right.mat';
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, alpha_source_stat)

colorbar('off');

view(45, 0) % right, lateral, back
material dull
camlight headlight

%--------------------------------------------------------------------------
%%                       do beta stat plot
%--------------------------------------------------------------------------
%% plot beta stat left, medial, bottom

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_stat color_lim_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, beta_source_stat)

% colorbar('off');

h = colorbar();
h.Ticks = [-color_lim_stat 0 color_lim_stat];
h.TickLength = 0;
h.FontName = font_name;
h.FontSize = 10;
h.Title.String = 'T';
h.Title.FontAngle = 'italic';
h.Title.FontSize = 10;
h.Title.FontName = font_name;
h.Box = 'off';

view(90, -45) % left, medial, bottom
material dull
camlight headlight

%% plot beta stat left, lateral, back

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_stat color_lim_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, beta_source_stat)

colorbar('off');

view(-45, 0) % left, lateral, back
material dull
camlight headlight

%% plot beta stat right, medial, bottom

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_stat color_lim_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_right.mat';
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, beta_source_stat)

colorbar('off');

view(-90, -45) % right, medial, bottom
material dull
camlight headlight

%% plot beta stat right, lateral, back

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_stat color_lim_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_right.mat';
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, beta_source_stat)

colorbar('off');

view(45, 0) % right, lateral, back
material dull
camlight headlight