%% init
str = [];

while isempty(str)
prompt = 'Please type or copy the path to your obob_ownft:';
str = input(prompt, 's');
end

addpath(str);
cfg = [];
obob_init_ft(cfg);

if ispc
    
    in_dir_fooof_induced = '.\data\fooof_results\';
    in_dir_fooof_evoked = '.\data\fooof_results_evoked\';
    addpath('.\utility_functions\');
    
else
    
    in_dir_fooof_induced = './data/fooof_results/';
    in_dir_fooof_evoked = './data/fooof_results_evoked/';
    addpath('./utility_functions/');
    
end

%% subject-list

subj_list = {
  '19890720ADLH';...
  '19950717ADTO';...
  '19960606MRRI';...
%   '19960817RGOE';... % excluded
  '19980530IIBR';...
  '19910823SSLD';...
  '19930506URHE';...
  '19980105PTLN';...
%   '19890227VRBA';... % excluded
  '19940613BTRI';... % #10
  '19920805CRLD';... 
  '19820101CRVL';...
  '19951106DRML';... 
  '19970218CRPO';... 
  '19990531IASH';...
  '19951209DRDN';...
  '19970714MRZN';...
  '19980704FBMF';...
  '19961106SBGI';...
%   '19950114URMR';... % #20 excluded
  '19970209SBRD';...
%   '19930630MNSU';... % excluded
%   '19900106DGBN';... % excluded
%   '19890714GBMN';... % excluded
%   '19962223CAHN';... % excluded
  '19891222GBHL';...
  '19910612CRKE';... 
  '19880705LHKO';...
  '19930630MNSU';...
  '19970421MRKL';... % #30
  '19990328SGRI';... 
  '19950508EISH';...
  '19960407MNSI';...
  '20010827MNMC';... % #34
  };

%--------------------------------------------------------------------------
%%                      stats for induced
%--------------------------------------------------------------------------

%% Load all subjects

Nsub = length(subj_list);
fooof_aud_left_induced = cell(Nsub, 1);
fooof_aud_right_induced = cell(Nsub, 1);
fooof_vis_left_induced = cell(Nsub, 1);
fooof_vis_right_induced = cell(Nsub, 1);

for ii = 1:Nsub
    
    % auditory left
    load(fullfile(in_dir_fooof_induced, sprintf('%s_fooof_aud_left.mat', subj_list{ii})));
    
    fooof_results_aud.freq = fooof_results_aud.freq_range(1):fooof_results_aud.freq_range(2);
    fooof_results_aud.dimord = 'chan_freq';
    fooof_results_aud.label = {'EEG001'};
    fooof_results_aud.slope = fooof_results_aud.background_params(2);
    fooof_results_aud.intercept = fooof_results_aud.background_params(1);
    fooof_results_aud.fooofed_spectrum = 10 .^ fooof_results_aud.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_aud.flattened_spectrum = 10 .^ fooof_results_aud.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_aud.original_spectrum = 10 .^ fooof_results_aud.original_spectrum; % reverse log-transform of fooof
    
    fooof_aud_left_induced{ii} = fooof_results_aud;
    
    clear fooof_results_aud
    
    % auditory right
    load(fullfile(in_dir_fooof_induced, sprintf('%s_fooof_aud_right.mat', subj_list{ii})));
    
    fooof_results_aud.freq = fooof_results_aud.freq_range(1):fooof_results_aud.freq_range(2);
    fooof_results_aud.dimord = 'chan_freq';
    fooof_results_aud.label = {'EEG002'};
    fooof_results_aud.slope = fooof_results_aud.background_params(2);
    fooof_results_aud.intercept = fooof_results_aud.background_params(1);
    fooof_results_aud.fooofed_spectrum = 10 .^ fooof_results_aud.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_aud.flattened_spectrum = 10 .^ fooof_results_aud.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_aud.original_spectrum = 10 .^ fooof_results_aud.original_spectrum; % reverse log-transform of fooof
    
    
    fooof_aud_right_induced{ii} = fooof_results_aud;
    
    % visual left
    load(fullfile(in_dir_fooof_induced, sprintf('%s_fooof_vis_left.mat', subj_list{ii})));
    
    fooof_results_vis.freq = fooof_results_vis.freq_range(1):fooof_results_vis.freq_range(2);
    fooof_results_vis.dimord = 'chan_freq';
    fooof_results_vis.label = {'EEG001'};
    fooof_results_vis.slope = fooof_results_vis.background_params(2);
    fooof_results_vis.intercept = fooof_results_vis.background_params(1);
    fooof_results_vis.fooofed_spectrum = 10 .^ fooof_results_vis.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_vis.flattened_spectrum = 10 .^ fooof_results_vis.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_vis.original_spectrum = 10 .^ fooof_results_vis.original_spectrum; % reverse log-transform of fooof
    
    fooof_vis_left_induced{ii} = fooof_results_vis;
    
    clear fooof_results_vis
    
    % visual right
    load(fullfile(in_dir_fooof_induced, sprintf('%s_fooof_vis_right.mat', subj_list{ii})));
    
    fooof_results_vis.freq = fooof_results_vis.freq_range(1):fooof_results_vis.freq_range(2);
    fooof_results_vis.dimord = 'chan_freq';
    fooof_results_vis.label = {'EEG002'};
    fooof_results_vis.slope = fooof_results_vis.background_params(2);
    fooof_results_vis.intercept = fooof_results_vis.background_params(1);
    fooof_results_vis.fooofed_spectrum = 10 .^ fooof_results_vis.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_vis.flattened_spectrum = 10 .^ fooof_results_vis.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_vis.original_spectrum = 10 .^ fooof_results_vis.original_spectrum; % reverse log-transform of fooof
    
    fooof_vis_right_induced{ii} = fooof_results_vis;
    
    clear fooof_results*
    
end

%% do contrast

nfreqs = length(fooof_aud_left_induced{1}.freq);

fooof_left_cntrst_induced = cell(Nsub, 1);
fooof_right_cntrst_induced = cell(Nsub, 1);
fooof_zeros_left_induced = cell(Nsub, 1);
fooof_zeros_right_induced = cell(Nsub, 1);

for ii = 1:Nsub
    
    fooof_left_cntrst_induced{ii} = fooof_aud_left_induced{ii};
    fooof_left_cntrst_induced{ii}.original_spectrum = ((fooof_aud_left_induced{ii}.original_spectrum - fooof_vis_left_induced{ii}.original_spectrum) ./ (fooof_aud_left_induced{ii}.original_spectrum + fooof_vis_left_induced{ii}.original_spectrum)) .* 100;
    fooof_left_cntrst_induced{ii}.flattened_spectrum = ((fooof_aud_left_induced{ii}.flattened_spectrum - fooof_vis_left_induced{ii}.flattened_spectrum) ./ (fooof_aud_left_induced{ii}.flattened_spectrum + fooof_vis_left_induced{ii}.flattened_spectrum)) .* 100;
    fooof_left_cntrst_induced{ii}.fooofed_spectrum = ((fooof_aud_left_induced{ii}.fooofed_spectrum - fooof_vis_left_induced{ii}.fooofed_spectrum) ./ (fooof_aud_left_induced{ii}.fooofed_spectrum + fooof_vis_left_induced{ii}.fooofed_spectrum)) .* 100;
    
    fooof_right_cntrst_induced{ii} = fooof_aud_right_induced{ii};
    fooof_right_cntrst_induced{ii}.original_spectrum = ((fooof_aud_right_induced{ii}.original_spectrum - fooof_vis_right_induced{ii}.original_spectrum) ./ (fooof_aud_right_induced{ii}.original_spectrum + fooof_vis_right_induced{ii}.original_spectrum)) .* 100;
    fooof_right_cntrst_induced{ii}.flattened_spectrum = ((fooof_aud_right_induced{ii}.flattened_spectrum - fooof_vis_right_induced{ii}.flattened_spectrum) ./ (fooof_aud_right_induced{ii}.flattened_spectrum + fooof_vis_right_induced{ii}.flattened_spectrum)) .* 100;
    fooof_right_cntrst_induced{ii}.fooofed_spectrum = ((fooof_aud_right_induced{ii}.fooofed_spectrum - fooof_vis_right_induced{ii}.fooofed_spectrum) ./ (fooof_aud_right_induced{ii}.fooofed_spectrum + fooof_vis_right_induced{ii}.fooofed_spectrum)) .* 100;
    
    fooof_zeros_left_induced{ii} = fooof_aud_left_induced{ii};
    fooof_zeros_left_induced{ii}.original_spectrum = zeros(1, nfreqs);
    fooof_zeros_left_induced{ii}.flattened_spectrum = zeros(1, nfreqs);
    fooof_zeros_left_induced{ii}.fooofed_spectrum = zeros(1, nfreqs);
    
    fooof_zeros_right_induced{ii} = fooof_aud_right_induced{ii};
    fooof_zeros_right_induced{ii}.original_spectrum = zeros(1, nfreqs);
    fooof_zeros_right_induced{ii}.flattened_spectrum = zeros(1, nfreqs);
    fooof_zeros_right_induced{ii}.fooofed_spectrum = zeros(1, nfreqs);
    
end

%% do stats for left

cfg = [];
cfg.parameter = 'fooofed_spectrum';
cfg.avgoverfreq = 'yes';
cfg.frequency = [3 10];
cfg.method = 'analytic';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.alpha = 0.05;
cfg.tail = 1;
 
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

stat_left_induced = ft_freqstatistics(cfg, fooof_left_cntrst_induced{:}, fooof_zeros_left_induced{:});

%% do stats for right

cfg = [];
cfg.parameter = 'fooofed_spectrum';
cfg.avgoverfreq = 'yes';
cfg.frequency = [1 10];
cfg.method = 'analytic';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.alpha = 0.05;
cfg.tail = 1;
 
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

stat_right_induced = ft_freqstatistics(cfg, fooof_right_cntrst_induced{:}, fooof_zeros_right_induced{:});

%% show results

fdr_induced = mafdr([stat_left_induced.prob, stat_right_induced.prob]);

stat_left_induced.prob = fdr_induced(1);
stat_right_induced.prob = fdr_induced(2);

% left
stat_left_induced.stat
stat_left_induced.prob

%right
stat_right_induced.stat
stat_right_induced.prob

%--------------------------------------------------------------------------
%%                      stats for evoked
%--------------------------------------------------------------------------

%% Load all subjects

Nsub = length(subj_list);
fooof_aud_left_evoked = cell(Nsub, 1);
fooof_aud_right_evoked = cell(Nsub, 1);
fooof_vis_left_evoked = cell(Nsub, 1);
fooof_vis_right_evoked = cell(Nsub, 1);

for ii = 1:Nsub
    
    % auditory left
    load(fullfile(in_dir_fooof_evoked, sprintf('%s_fooof_aud_left.mat', subj_list{ii})));
    
    fooof_results_aud.freq = fooof_results_aud.freq_range(1):fooof_results_aud.freq_range(2);
    fooof_results_aud.dimord = 'chan_freq';
    fooof_results_aud.label = {'EEG001'};
    fooof_results_aud.slope = fooof_results_aud.background_params(2);
    fooof_results_aud.intercept = fooof_results_aud.background_params(1);
    fooof_results_aud.fooofed_spectrum = 10 .^ fooof_results_aud.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_aud.flattened_spectrum = 10 .^ fooof_results_aud.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_aud.original_spectrum = 10 .^ fooof_results_aud.original_spectrum; % reverse log-transform of fooof
    
    fooof_aud_left_evoked{ii} = fooof_results_aud;
    
    clear fooof_results_aud
    
    % auditory right
    load(fullfile(in_dir_fooof_evoked, sprintf('%s_fooof_aud_right.mat', subj_list{ii})));
    
    fooof_results_aud.freq = fooof_results_aud.freq_range(1):fooof_results_aud.freq_range(2);
    fooof_results_aud.dimord = 'chan_freq';
    fooof_results_aud.label = {'EEG002'};
    fooof_results_aud.slope = fooof_results_aud.background_params(2);
    fooof_results_aud.intercept = fooof_results_aud.background_params(1);
    fooof_results_aud.fooofed_spectrum = 10 .^ fooof_results_aud.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_aud.flattened_spectrum = 10 .^ fooof_results_aud.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_aud.original_spectrum = 10 .^ fooof_results_aud.original_spectrum; % reverse log-transform of fooof
    
    
    fooof_aud_right_evoked{ii} = fooof_results_aud;
    
    % visual left
    load(fullfile(in_dir_fooof_evoked, sprintf('%s_fooof_vis_left.mat', subj_list{ii})));
    
    fooof_results_vis.freq = fooof_results_vis.freq_range(1):fooof_results_vis.freq_range(2);
    fooof_results_vis.dimord = 'chan_freq';
    fooof_results_vis.label = {'EEG001'};
    fooof_results_vis.slope = fooof_results_vis.background_params(2);
    fooof_results_vis.intercept = fooof_results_vis.background_params(1);
    fooof_results_vis.fooofed_spectrum = 10 .^ fooof_results_vis.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_vis.flattened_spectrum = 10 .^ fooof_results_vis.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_vis.original_spectrum = 10 .^ fooof_results_vis.original_spectrum; % reverse log-transform of fooof
    
    fooof_vis_left_evoked{ii} = fooof_results_vis;
    
    clear fooof_results_vis
    
    % visual right
    load(fullfile(in_dir_fooof_evoked, sprintf('%s_fooof_vis_right.mat', subj_list{ii})));
    
    fooof_results_vis.freq = fooof_results_vis.freq_range(1):fooof_results_vis.freq_range(2);
    fooof_results_vis.dimord = 'chan_freq';
    fooof_results_vis.label = {'EEG002'};
    fooof_results_vis.slope = fooof_results_vis.background_params(2);
    fooof_results_vis.intercept = fooof_results_vis.background_params(1);
    fooof_results_vis.fooofed_spectrum = 10 .^ fooof_results_vis.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_vis.flattened_spectrum = 10 .^ fooof_results_vis.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_vis.original_spectrum = 10 .^ fooof_results_vis.original_spectrum; % reverse log-transform of fooof
    
    fooof_vis_right_evoked{ii} = fooof_results_vis;
    
    clear fooof_results*
    
end

%% do contrast

nfreqs = length(fooof_aud_left_evoked{1}.freq);

fooof_left_cntrst_evoked = cell(Nsub, 1);
fooof_right_cntrst_evoked = cell(Nsub, 1);
fooof_zeros_left_evoked = cell(Nsub, 1);
fooof_zeros_right_evoked = cell(Nsub, 1);

for ii = 1:Nsub
    
    fooof_left_cntrst_evoked{ii} = fooof_aud_left_evoked{ii};
    fooof_left_cntrst_evoked{ii}.original_spectrum = ((fooof_aud_left_evoked{ii}.original_spectrum - fooof_vis_left_evoked{ii}.original_spectrum) ./ (fooof_aud_left_evoked{ii}.original_spectrum + fooof_vis_left_evoked{ii}.original_spectrum)) .* 100;
    fooof_left_cntrst_evoked{ii}.flattened_spectrum = ((fooof_aud_left_evoked{ii}.flattened_spectrum - fooof_vis_left_evoked{ii}.flattened_spectrum) ./ (fooof_aud_left_evoked{ii}.flattened_spectrum + fooof_vis_left_evoked{ii}.flattened_spectrum)) .* 100;
    fooof_left_cntrst_evoked{ii}.fooofed_spectrum = ((fooof_aud_left_evoked{ii}.fooofed_spectrum - fooof_vis_left_evoked{ii}.fooofed_spectrum) ./ (fooof_aud_left_evoked{ii}.fooofed_spectrum + fooof_vis_left_evoked{ii}.fooofed_spectrum)) .* 100;
    
    fooof_right_cntrst_evoked{ii} = fooof_aud_right_evoked{ii};
    fooof_right_cntrst_evoked{ii}.original_spectrum = ((fooof_aud_right_evoked{ii}.original_spectrum - fooof_vis_right_evoked{ii}.original_spectrum) ./ (fooof_aud_right_evoked{ii}.original_spectrum + fooof_vis_right_evoked{ii}.original_spectrum)) .* 100;
    fooof_right_cntrst_evoked{ii}.flattened_spectrum = ((fooof_aud_right_evoked{ii}.flattened_spectrum - fooof_vis_right_evoked{ii}.flattened_spectrum) ./ (fooof_aud_right_evoked{ii}.flattened_spectrum + fooof_vis_right_evoked{ii}.flattened_spectrum)) .* 100;
    fooof_right_cntrst_evoked{ii}.fooofed_spectrum = ((fooof_aud_right_evoked{ii}.fooofed_spectrum - fooof_vis_right_evoked{ii}.fooofed_spectrum) ./ (fooof_aud_right_evoked{ii}.fooofed_spectrum + fooof_vis_right_evoked{ii}.fooofed_spectrum)) .* 100;
    
    fooof_zeros_left_evoked{ii} = fooof_aud_left_evoked{ii};
    fooof_zeros_left_evoked{ii}.original_spectrum = zeros(1, nfreqs);
    fooof_zeros_left_evoked{ii}.flattened_spectrum = zeros(1, nfreqs);
    fooof_zeros_left_evoked{ii}.fooofed_spectrum = zeros(1, nfreqs);
    
    fooof_zeros_right_evoked{ii} = fooof_aud_right_evoked{ii};
    fooof_zeros_right_evoked{ii}.original_spectrum = zeros(1, nfreqs);
    fooof_zeros_right_evoked{ii}.flattened_spectrum = zeros(1, nfreqs);
    fooof_zeros_right_evoked{ii}.fooofed_spectrum = zeros(1, nfreqs);
    
end

%% do stats for left

cfg = [];
cfg.parameter = 'fooofed_spectrum';
cfg.avgoverfreq = 'yes';
cfg.frequency = [1 9];
cfg.method = 'analytic';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.alpha = 0.05;
cfg.tail = 1;
 
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

stat_left_evoked = ft_freqstatistics(cfg, fooof_left_cntrst_evoked{:}, fooof_zeros_left_evoked{:});

%% do stats for right

cfg = [];
cfg.parameter = 'fooofed_spectrum';
cfg.avgoverfreq = 'yes';
cfg.frequency = [1 8];
cfg.method = 'analytic';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.alpha = 0.05;
cfg.tail = 1;
 
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

stat_right_evoked = ft_freqstatistics(cfg, fooof_right_cntrst_evoked{:}, fooof_zeros_right_evoked{:});

%% show results

fdr_evoked = mafdr([stat_left_evoked.prob, stat_right_evoked.prob]);

stat_left_evoked.prob = fdr_evoked(1);
stat_right_evoked.prob = fdr_evoked(2);

% left
stat_left_evoked.stat
stat_left_evoked.prob

%right
stat_right_evoked.stat
stat_right_evoked.prob