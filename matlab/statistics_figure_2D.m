%% init
str = [];

while isempty(str)
prompt = 'Please type or copy the path to your obob_ownft:';
str = input(prompt, 's');
end

addpath(str);
cfg = [];
cfg.package.svs = true;
obob_init_ft(cfg);

% load grid and convert to m
load mni_grid_1_cm_2982pnts
template_grid = ft_convert_units(template_grid,'m');

if ispc
    
    in_dir_source = '.\data\source\';
    addpath('.\utility_functions\');
    
else
    
    in_dir_source = './data/source/';
    addpath('./utility_functions/');
    
end

%% subject-list

subj_list = {
  '19890720ADLH';...
  '19950717ADTO';...
  '19960606MRRI';...
%   '19960817RGOE';... % excluded
  '19980530IIBR';...
  '19910823SSLD';...
  '19930506URHE';...
  '19980105PTLN';...
%   '19890227VRBA';... % excluded
  '19940613BTRI';... % #10
  '19920805CRLD';... 
  '19820101CRVL';...
  '19951106DRML';... 
  '19970218CRPO';... 
  '19990531IASH';...
  '19951209DRDN';...
  '19970714MRZN';...
  '19980704FBMF';...
  '19961106SBGI';...
%   '19950114URMR';... % #20 excluded
  '19970209SBRD';...
%   '19930630MNSU';... % excluded
%   '19900106DGBN';... % excluded
%   '19890714GBMN';... % excluded
%   '19962223CAHN';... % excluded
  '19891222GBHL';...
  '19910612CRKE';... 
  '19880705LHKO';...
  '19930630MNSU';...
  '19970421MRKL';... % #30
  '19990328SGRI';... 
  '19950508EISH';...
  '19960407MNSI';...
  '20010827MNMC';... % #34
  };

%% load data

tfr_all_aud = cell(length(subj_list), 1);
tfr_all_vis = cell(length(subj_list), 1);
tfr_all_cntrst = cell(length(subj_list), 1);

for ii = 1:length(subj_list)
  
  subj_name = subj_list{ii};
  
  load([in_dir_source, subj_name, '_tfr_clean_regfac5.mat']);
  
  tfr_all_aud{ii} = tfr_aud;
  tfr_all_vis{ii} = tfr_vis;
  
  clear tfr_aud tfr_vis
  
end

%% cluster statistics for MEG

cfg = [];
cfg.latency = [-1.75 -0.05];
cfg.avgovertime = 'yes';
cfg.frequency = [3 25];
cfg.parameter = 'powspctrm';
cfg.method = 'montecarlo';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.alpha = 0.05;
cfg.correctm = 'cluster';
cfg.clusterthreshold = 'nonparametric_individual';
cfg.numrandomization = 1000;
cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.tail = 0;
cfg.clustertail = 0;
cfg.correcttail = 'prob';

% prepare neighbours
elec = [];
elec.elecpos = template_grid.pos(template_grid.inside, :);
elec.label = tfr_all_aud{1}.label;

cfg_neighb.method = 'distance';
cfg_neighb.elec = elec;
cfg_neighb.neighbourdist = 0.02;
cfg.neighbours = ft_prepare_neighbours(cfg_neighb);
 
[Nsub, ~] = size(tfr_all_aud);
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

cluster_stat = ft_freqstatistics(cfg, tfr_all_aud{:}, tfr_all_vis{:});

%--------------------------------------------------------------------------
%%                       do alpha-range stat plot
%--------------------------------------------------------------------------
%% prepare alpha stat plot

% virtual sens to source
cfg = [];
cfg.sourcegrid = template_grid;
cfg.parameter = 'stat';
cfg.mri = mri;
cfg.latency = [-1.75 -0.05];
cfg.frequency = [10 11];
alpha_source_stat = obob_svs_virtualsens2source(cfg, cluster_stat);

cfg.parameter = 'mask'; % posclusterslabelmat
source_mask = obob_svs_virtualsens2source(cfg, cluster_stat);

alpha_source_stat.mask = source_mask.mask;

max_alpha_source_stat = round(max(max(max(alpha_source_stat.stat))));
min_abs_alpha_source_stat = round(abs(min(min(min(alpha_source_stat.stat)))));

if max_alpha_source_stat > min_abs_alpha_source_stat
  
  color_lim_alpha_stat = max_alpha_source_stat;
  
else
  
  color_lim_alpha_stat = min_abs_alpha_source_stat;
  
end

%% plot alpha stat left, medial, bottom

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_alpha_stat color_lim_alpha_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, alpha_source_stat)

colorbar('off');

% h = colorbar();
% h.Ticks = [-color_lim_alpha_stat 0 color_lim_alpha_stat];
% h.TickLength = 0;
% h.FontName = font_name;
% h.FontSize = 10;
% h.Title.String = 'T';
% h.Title.FontAngle = 'italic';
% h.Title.FontSize = 10;
% h.Title.FontName = font_name;
% h.Box = 'off';

view(90, -45) % left, medial, bottom
material dull
camlight headlight

%% plot alpha stat left, lateral, back

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_alpha_stat color_lim_alpha_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, alpha_source_stat)

colorbar('off');

view(-45, 0) % left, lateral, back
material dull
camlight headlight

%% plot alpha stat right, medial, bottom

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_alpha_stat color_lim_alpha_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_right.mat';
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, alpha_source_stat)

colorbar('off');

view(-90, -45) % right, medial, bottom
material dull
camlight headlight

%% plot alpha stat right, lateral, back

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_alpha_stat color_lim_alpha_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_right.mat';
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, alpha_source_stat)

colorbar('off');

view(45, 0) % right, lateral, back
material dull
camlight headlight

%--------------------------------------------------------------------------
%%                       do beta-range stat plot
%--------------------------------------------------------------------------
%% prepare beta stat plot

% virtual sens to source
cfg = [];
cfg.sourcegrid = template_grid;
cfg.parameter = 'stat';
cfg.mri = mri;
cfg.latency = [-1.75 -0.05];
cfg.frequency = [16 18];
beta_source_stat = obob_svs_virtualsens2source(cfg, cluster_stat);

cfg.parameter = 'mask'; % posclusterslabelmat
source_mask = obob_svs_virtualsens2source(cfg, cluster_stat);

beta_source_stat.mask = source_mask.mask;

max_beta_source_stat = round(max(max(max(beta_source_stat.stat))));
min_abs_beta_source_stat = round(abs(min(min(min(beta_source_stat.stat)))));

if max_beta_source_stat > min_abs_beta_source_stat
  
  color_lim_beta_stat = max_beta_source_stat;
  
else
  
  color_lim_beta_stat = min_abs_beta_source_stat;
  
end

%% plot beta stat left, medial, bottom

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_beta_stat color_lim_beta_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, beta_source_stat)

colorbar('off');

% h = colorbar();
% h.Ticks = [-color_lim_beta_stat 0 color_lim_beta_stat];
% h.TickLength = 0;
% h.FontName = font_name;
% h.FontSize = 10;
% h.Title.String = 'T';
% h.Title.FontAngle = 'italic';
% h.Title.FontSize = 10;
% h.Title.FontName = font_name;
% h.Box = 'off';

view(90, -45) % left, medial, bottom
material dull
camlight headlight

%% plot beta stat left, lateral, back

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_beta_stat color_lim_beta_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, beta_source_stat)

colorbar('off');

view(-45, 0) % left, lateral, back
material dull
camlight headlight

%% plot beta stat right, medial, bottom

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_beta_stat color_lim_beta_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_right.mat';
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, beta_source_stat)

colorbar('off');

view(-90, -45) % right, medial, bottom
material dull
camlight headlight

%% plot beta stat right, lateral, back

cfg = [];
cfg.funparameter = 'stat';
cfg.method = 'surface';
cfg.projmethod     = 'nearest'; 
cfg.maskparameter = 'mask';
cfg.funcolormap = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
cfg.funcolorlim = [-color_lim_beta_stat color_lim_beta_stat];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_right.mat';
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, beta_source_stat)

colorbar('off');

view(45, 0) % right, lateral, back
material dull
camlight headlight
