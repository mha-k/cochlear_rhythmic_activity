%% init

if ispc
    
    in_dir_ooa_phase = '.\data\ooa_phase\';
    addpath('.\utility_functions\circstat-matlab\');
    
else
    
    in_dir_ooa_phase = './data/ooa_phase/';
    addpath('./utility_functions/circstat-matlab/');
    
end

%% load data

degrees = readtable([in_dir_ooa_phase, 'ooa_degrees_timelocked.csv']);
degrees.left_degrees = deg2rad(degrees.left_degrees);
degrees.right_degrees = deg2rad(degrees.right_degrees);

%% calculate circular means

mean_left = circ_mean([degrees.left_degrees(strcmp(degrees.condition, 'auditory')), degrees.left_degrees(strcmp(degrees.condition, 'visual'))], [], 2);
mean_right = circ_mean([degrees.right_degrees(strcmp(degrees.condition, 'auditory')), degrees.right_degrees(strcmp(degrees.condition, 'visual'))], [], 2);

mean_attention = circ_mean([degrees.left_degrees, degrees.right_degrees], [], 2);

%% calculate circular common median tests

[p_attention, ~, statistic_attention] = circ_cmtest(mean_attention(1:27), mean_attention(28:end));

[p_ear, ~, statistic_ear] = circ_cmtest(mean_left, mean_right);