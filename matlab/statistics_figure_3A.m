%% init
str = [];

while isempty(str)
prompt = 'Please type or copy the path to your obob_ownft:';
str = input(prompt, 's');
end

addpath(str);
cfg = [];
cfg.package.svs = true;
cfg.package.obsolete = 'true';
cfg.package.gm2 = 'true';
obob_init_ft(cfg);

if ispc
    
    in_dir_source = '.\data\source\';
    in_dir_fooof = '.\data\fooof_results\';
    atlas = ft_read_atlas([str '\external\fieldtrip\template\atlas\aal\ROI_MNI_V4.nii']);
    addpath('.\utility_functions\');
    
else
    
    in_dir_source = './data/source/';
    in_dir_fooof = './data/fooof_results/';
    atlas = ft_read_atlas([str '/external/fieldtrip/template/atlas/aal/ROI_MNI_V4.nii']);
    addpath('./utility_functions/');
    
end

% load grid and convert to m
load mni_grid_1_cm_2982pnts
template_grid = ft_convert_units(template_grid,'m');

% convert atlas to m
atlas = ft_convert_units(atlas,'m');

% load mri
load standard_mri_better
mri = ft_convert_units(mri, 'm');

%% subject-list

subj_list = {
  '19890720ADLH';...
  '19950717ADTO';...
  '19960606MRRI';...
%   '19960817RGOE';... % excluded
  '19980530IIBR';...
  '19910823SSLD';...
  '19930506URHE';...
  '19980105PTLN';...
%   '19890227VRBA';... % excluded
  '19940613BTRI';... % #10
  '19920805CRLD';... 
  '19820101CRVL';...
  '19951106DRML';... 
  '19970218CRPO';... 
  '19990531IASH';...
  '19951209DRDN';...
  '19970714MRZN';...
  '19980704FBMF';...
  '19961106SBGI';...
%   '19950114URMR';... % #20 excluded
  '19970209SBRD';...
%   '19930630MNSU';... % excluded
%   '19900106DGBN';... % excluded
%   '19890714GBMN';... % excluded
%   '19962223CAHN';... % excluded
  '19891222GBHL';...
  '19910612CRKE';... 
  '19880705LHKO';...
  '19930630MNSU';...
  '19970421MRKL';... % #30
  '19990328SGRI';... 
  '19950508EISH';...
  '19960407MNSI';...
  '20010827MNMC';... % #34
  };

%% load data

% MEG
meg_all_cntrst = cell(length(subj_list), 1);

for ii = 1:length(subj_list)
  
  subj_name = subj_list{ii};
  
  load([in_dir_source, subj_name, '_tfr_clean_regfac5.mat']); % lfFromBlock05.mat / clean.mat / clean_regfac5.mat
  
  meg_all_cntrst{ii} = tfr_aud;
  meg_all_cntrst{ii}.powspctrm = (tfr_aud.powspctrm - tfr_vis.powspctrm) ./ (tfr_aud.powspctrm + tfr_vis.powspctrm) .* 100;
  
  clear tfr_aud tfr_vis
  
end

% load FOOOF results for separated by chans
Nsub = length(subj_list);
fooof_aud_left = cell(Nsub, 1);
fooof_aud_right = cell(Nsub, 1);
fooof_vis_left = cell(Nsub, 1);
fooof_vis_right = cell(Nsub, 1);

for ii = 1:Nsub
    
    % auditory left
    load(fullfile(in_dir_fooof, sprintf('%s_fooof_aud_left.mat', subj_list{ii})));
    
    fooof_results_aud.freq = fooof_results_aud.freq_range(1):fooof_results_aud.freq_range(2);
    fooof_results_aud.dimord = 'chan_freq';
    fooof_results_aud.label = {'EEG001'};
    fooof_results_aud.slope = fooof_results_aud.background_params(2);
    fooof_results_aud.intercept = fooof_results_aud.background_params(1);
    fooof_results_aud.fooofed_spectrum = 10 .^ fooof_results_aud.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_aud.flattened_spectrum = 10 .^ fooof_results_aud.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_aud.original_spectrum = 10 .^ fooof_results_aud.original_spectrum; % reverse log-transform of fooof
    
    fooof_aud_left{ii} = fooof_results_aud;
    
    clear fooof_results_aud
    
    % auditory right
    load(fullfile(in_dir_fooof, sprintf('%s_fooof_aud_right.mat', subj_list{ii})));
    
    fooof_results_aud.freq = fooof_results_aud.freq_range(1):fooof_results_aud.freq_range(2);
    fooof_results_aud.dimord = 'chan_freq';
    fooof_results_aud.label = {'EEG002'};
    fooof_results_aud.slope = fooof_results_aud.background_params(2);
    fooof_results_aud.intercept = fooof_results_aud.background_params(1);
    fooof_results_aud.fooofed_spectrum = 10 .^ fooof_results_aud.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_aud.flattened_spectrum = 10 .^ fooof_results_aud.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_aud.original_spectrum = 10 .^ fooof_results_aud.original_spectrum; % reverse log-transform of fooof
    
    
    fooof_aud_right{ii} = fooof_results_aud;
    
    % visual left
    load(fullfile(in_dir_fooof, sprintf('%s_fooof_vis_left.mat', subj_list{ii})));
    
    fooof_results_vis.freq = fooof_results_vis.freq_range(1):fooof_results_vis.freq_range(2);
    fooof_results_vis.dimord = 'chan_freq';
    fooof_results_vis.label = {'EEG001'};
    fooof_results_vis.slope = fooof_results_vis.background_params(2);
    fooof_results_vis.intercept = fooof_results_vis.background_params(1);
    fooof_results_vis.fooofed_spectrum = 10 .^ fooof_results_vis.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_vis.flattened_spectrum = 10 .^ fooof_results_vis.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_vis.original_spectrum = 10 .^ fooof_results_vis.original_spectrum; % reverse log-transform of fooof
    
    fooof_vis_left{ii} = fooof_results_vis;
    
    clear fooof_results_vis
    
    % visual right
    load(fullfile(in_dir_fooof, sprintf('%s_fooof_vis_right.mat', subj_list{ii})));
    
    fooof_results_vis.freq = fooof_results_vis.freq_range(1):fooof_results_vis.freq_range(2);
    fooof_results_vis.dimord = 'chan_freq';
    fooof_results_vis.label = {'EEG002'};
    fooof_results_vis.slope = fooof_results_vis.background_params(2);
    fooof_results_vis.intercept = fooof_results_vis.background_params(1);
    fooof_results_vis.fooofed_spectrum = 10 .^ fooof_results_vis.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_vis.flattened_spectrum = 10 .^ fooof_results_vis.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_vis.original_spectrum = 10 .^ fooof_results_vis.original_spectrum; % reverse log-transform of fooof
    
    fooof_vis_right{ii} = fooof_results_vis;
    
    clear fooof_results*
    
end

%% FOOOF contrast
nfreqs = length(fooof_aud_left{1}.freq);

fooof_left_cntrst = cell(Nsub, 1);
fooof_right_cntrst = cell(Nsub, 1);

for ii = 1:Nsub
    
    fooof_left_cntrst{ii} = fooof_aud_left{ii};
    fooof_left_cntrst{ii}.original_spectrum = ((fooof_aud_left{ii}.original_spectrum - fooof_vis_left{ii}.original_spectrum) ./ (fooof_aud_left{ii}.original_spectrum + fooof_vis_left{ii}.original_spectrum)) .* 100;
    fooof_left_cntrst{ii}.flattened_spectrum = ((fooof_aud_left{ii}.flattened_spectrum - fooof_vis_left{ii}.flattened_spectrum) ./ (fooof_aud_left{ii}.flattened_spectrum + fooof_vis_left{ii}.flattened_spectrum)) .* 100;
    fooof_left_cntrst{ii}.fooofed_spectrum = ((fooof_aud_left{ii}.fooofed_spectrum - fooof_vis_left{ii}.fooofed_spectrum) ./ (fooof_aud_left{ii}.fooofed_spectrum + fooof_vis_left{ii}.fooofed_spectrum)) .* 100;
    
    fooof_right_cntrst{ii} = fooof_aud_right{ii};
    fooof_right_cntrst{ii}.original_spectrum = ((fooof_aud_right{ii}.original_spectrum - fooof_vis_right{ii}.original_spectrum) ./ (fooof_aud_right{ii}.original_spectrum + fooof_vis_right{ii}.original_spectrum)) .* 100;
    fooof_right_cntrst{ii}.flattened_spectrum = ((fooof_aud_right{ii}.flattened_spectrum - fooof_vis_right{ii}.flattened_spectrum) ./ (fooof_aud_right{ii}.flattened_spectrum + fooof_vis_right{ii}.flattened_spectrum)) .* 100;
    fooof_right_cntrst{ii}.fooofed_spectrum = ((fooof_aud_right{ii}.fooofed_spectrum - fooof_vis_right{ii}.fooofed_spectrum) ./ (fooof_aud_right{ii}.fooofed_spectrum + fooof_vis_right{ii}.fooofed_spectrum)) .* 100;
    
end

%% prepare FOOOF

min_mean_freq = 1;
max_mean_freq = 10;

for ii = 1:Nsub
    
    corr_fooof_right(:, ii) = mean(fooof_right_cntrst{ii}.fooofed_spectrum(min_mean_freq:max_mean_freq));
    
end

%% cluster staistics

cfg = [];
cfg.latency = [-1.75 -0.05];
cfg.frequency = [16 18];
cfg.avgoverfreq = 'yes';
cfg.parameter = 'powspctrm';
cfg.method = 'montecarlo';
cfg.statistic = 'ft_statfun_correlationT';
cfg.type = 'Spearman';
cfg.alpha = 0.05;
cfg.correctm = 'cluster';
cfg.clusterthreshold = 'nonparametric_individual';
cfg.numrandomization = 1000;
cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.tail = 0;
cfg.clustertail = 0;
cfg.correcttail = 'prob';

% prepare neighbours
elec = [];
elec.elecpos = template_grid.pos(template_grid.inside, :);
elec.label = meg_all_cntrst{1}.label;

cfg_neighb.method = 'distance';
cfg_neighb.elec = elec;
cfg_neighb.neighbourdist = 0.02;
cfg.neighbours = ft_prepare_neighbours(cfg_neighb);
 
cfg.design(1,1:Nsub)  = corr_fooof_right;
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable

corr_stat_right = ft_freqstatistics(cfg, meg_all_cntrst{:});

%--------------------------------------------------------------------------
%%                       plot surface for alpha
%--------------------------------------------------------------------------
%% virtual sens to source

cfg = [];
cfg.sourcegrid = template_grid; % new_grid for ROI
cfg.parameter = 'stat';
%cfg.maskparameter = 'mask';
cfg.mri = mri;
cfg.latency = [corr_stat_right.time corr_stat_right.time];
cfg.atlas = atlas;
% cfg.frequency = [stat_left.freq stat_left.freq];
cfg.frequency = [11 12]; % theta [6 6], alpha [11 12], beta [19 21]
source_corr_stat_right = obob_svs_virtualsens2source(cfg, corr_stat_right);

% mask for plot
source_corr_stat_right.stat2 = niftiread([in_dir_source, 'alpha_corr_mask_thresh75.nii']);

%% plot left lateral

cfg = [];
cfg.funparameter = 'stat2';
cfg.method = 'surface';
cfg.maskparameter = 'stat2';
cfg.projmethod = 'nearest';
cfg.funcolormap = [0.2941 0 0.1765]; % color for alpha-band
% cfg.funcolorlim = [-8 8];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, source_corr_stat_right)

colorbar('off');

view(-90, 0) % lateral
material dull
camlight headlight

%% plot left medial

ft_sourceplot(cfg, source_corr_stat_right)

colorbar('off');

view(90, 0) % medial
material dull
camlight headlight

%% plot right medial

cfg.surffile = 'surface_white_right.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, source_corr_stat_right)

colorbar('off');

view(-90, 0) % medial
material dull
camlight headlight

%% plot right lateral

cfg.surffile = 'surface_white_right.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_right_caret.mat';

ft_sourceplot(cfg, source_corr_stat_right)

colorbar('off');

view(90, 0) % lateral
material dull
camlight headlight

%--------------------------------------------------------------------------
%%                       plot surface for theta
%--------------------------------------------------------------------------
%% virtual sens to source

cfg = [];
cfg.sourcegrid = template_grid; % new_grid for ROI
cfg.parameter = 'stat';
%cfg.maskparameter = 'mask';
cfg.mri = mri;
cfg.latency = [corr_stat_right.time corr_stat_right.time];
cfg.atlas = atlas;
% cfg.frequency = [corr_stat_right.freq corr_stat_right.freq];
cfg.frequency = [6 6]; % theta [6 6], alpha [11 12], beta [19 21]
source_corr_stat_right = obob_svs_virtualsens2source(cfg, corr_stat_right);

% mask for plot
source_corr_stat_right.stat2 = niftiread([in_dir_source, 'theta_corr_mask_thresh75.nii']);

%% plot left lateral

cfg = [];
cfg.funparameter = 'stat2';
cfg.method = 'surface';
cfg.maskparameter = 'stat2';
cfg.projmethod = 'nearest';
cfg.funcolormap = [0.0078 0.8235 0.9882]; % color for theta-band
% cfg.funcolorlim = [-8 8];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, source_corr_stat_right)

colorbar('off');

view(-90, 0) % lateral
material dull
camlight headlight

%% plot left medial

ft_sourceplot(cfg, source_corr_stat_right)

colorbar('off');

view(90, 0) % medial
material dull
camlight headlight

%--------------------------------------------------------------------------
%%                       plot surface for beta
%--------------------------------------------------------------------------
%% virtual sens to source

cfg = [];
cfg.sourcegrid = template_grid; % new_grid for ROI
cfg.parameter = 'stat';
%cfg.maskparameter = 'mask';
cfg.mri = mri;
cfg.latency = [corr_stat_right.time corr_stat_right.time];
cfg.atlas = atlas;
% cfg.frequency = [corr_stat_right.freq corr_stat_right.freq];
cfg.frequency = [19 21]; % theta [6 6], alpha [11 12], beta [19 21]
source_corr_stat_right = obob_svs_virtualsens2source(cfg, corr_stat_right);

% mask for plot
source_corr_stat_right.stat2 = niftiread([in_dir_source, 'beta_corr_mask_thresh75.nii']);

%% plot left lateral

cfg = [];
cfg.funparameter = 'stat2';
cfg.method = 'surface';
cfg.maskparameter = 'stat2';
cfg.projmethod = 'nearest';
cfg.funcolormap = [0.0980 0.3922 0.1529]; % color for theta-band
% cfg.funcolorlim = [-8 8];
cfg.camlight = 'no';
cfg.surffile = 'surface_white_left.mat'; % get this file for nicely plotting inflated brains
cfg.surfinflated = 'surface_inflated_left_caret.mat';

ft_sourceplot(cfg, source_corr_stat_right)

colorbar('off');

view(-90, 0) % lateral
material dull
camlight headlight

%% plot left medial

ft_sourceplot(cfg, source_corr_stat_right)

colorbar('off');

view(90, 0) % medial
material dull
camlight headlight
