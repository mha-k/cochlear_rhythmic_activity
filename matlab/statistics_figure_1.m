%% init
str = [];

while isempty(str)
prompt = 'Please type or copy the path to your obob_ownft:';
str = input(prompt, 's');
end

addpath(str);
cfg = [];
obob_init_ft(cfg);

%% get subject list

subj_list = {
  '19890720ADLH';...
  '19950717ADTO';...
  '19960606MRRI';...
%   '19960817RGOE';... % excluded
  '19980530IIBR';...
  '19910823SSLD';...
  '19930506URHE';...
  '19980105PTLN';...
%   '19890227VRBA';... % excluded
  '19940613BTRI';... % #10
  '19920805CRLD';... 
  '19820101CRVL';...
  '19951106DRML';... 
  '19970218CRPO';... 
  '19990531IASH';...
  '19951209DRDN';...
  '19970714MRZN';...
  '19980704FBMF';...
  '19961106SBGI';...
%   '19950114URMR';... % #20 excluded
  '19970209SBRD';...
%   '19930630MNSU';... % excluded
%   '19900106DGBN';... % excluded
%   '19890714GBMN';... % excluded
%   '19962223CAHN';... % excluded
  '19891222GBHL';...
  '19910612CRKE';... 
  '19880705LHKO';...
  '19930630MNSU';...
  '19970421MRKL';... % #30
  '19990328SGRI';... 
  '19950508EISH';...
  '19960407MNSI';...
  '20010827MNMC';... % #34
  };

%% Load all subjects

if ispc
    
    in_dir_fooof = '.\data\fooof_results\';
    
else
    
    in_dir_fooof = './data/fooof_results/';
    
end

Nsub = length(subj_list);
fooof_aud_left = cell(Nsub, 1);
fooof_aud_right = cell(Nsub, 1);
fooof_vis_left = cell(Nsub, 1);
fooof_vis_right = cell(Nsub, 1);

for ii = 1:Nsub
    
    % auditory left
    load(fullfile(in_dir_fooof, sprintf('%s_fooof_aud_left.mat', subj_list{ii})));
    
    fooof_results_aud.freq = fooof_results_aud.freq_range(1):fooof_results_aud.freq_range(2);
    fooof_results_aud.dimord = 'chan_freq';
    fooof_results_aud.label = {'EEG001'};
    fooof_results_aud.slope = fooof_results_aud.background_params(2);
    fooof_results_aud.intercept = fooof_results_aud.background_params(1);
    fooof_results_aud.fooofed_spectrum = 10 .^ fooof_results_aud.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_aud.flattened_spectrum = 10 .^ fooof_results_aud.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_aud.original_spectrum = 10 .^ fooof_results_aud.original_spectrum; % reverse log-transform of fooof
    
    fooof_aud_left{ii} = fooof_results_aud;
    
    clear fooof_results_aud
    
    % auditory right
    load(fullfile(in_dir_fooof, sprintf('%s_fooof_aud_right.mat', subj_list{ii})));
    
    fooof_results_aud.freq = fooof_results_aud.freq_range(1):fooof_results_aud.freq_range(2);
    fooof_results_aud.dimord = 'chan_freq';
    fooof_results_aud.label = {'EEG002'};
    fooof_results_aud.slope = fooof_results_aud.background_params(2);
    fooof_results_aud.intercept = fooof_results_aud.background_params(1);
    fooof_results_aud.fooofed_spectrum = 10 .^ fooof_results_aud.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_aud.flattened_spectrum = 10 .^ fooof_results_aud.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_aud.original_spectrum = 10 .^ fooof_results_aud.original_spectrum; % reverse log-transform of fooof
    
    
    fooof_aud_right{ii} = fooof_results_aud;
    
    % visual left
    load(fullfile(in_dir_fooof, sprintf('%s_fooof_vis_left.mat', subj_list{ii})));
    
    fooof_results_vis.freq = fooof_results_vis.freq_range(1):fooof_results_vis.freq_range(2);
    fooof_results_vis.dimord = 'chan_freq';
    fooof_results_vis.label = {'EEG001'};
    fooof_results_vis.slope = fooof_results_vis.background_params(2);
    fooof_results_vis.intercept = fooof_results_vis.background_params(1);
    fooof_results_vis.fooofed_spectrum = 10 .^ fooof_results_vis.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_vis.flattened_spectrum = 10 .^ fooof_results_vis.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_vis.original_spectrum = 10 .^ fooof_results_vis.original_spectrum; % reverse log-transform of fooof
    
    fooof_vis_left{ii} = fooof_results_vis;
    
    clear fooof_results_vis
    
    % visual right
    load(fullfile(in_dir_fooof, sprintf('%s_fooof_vis_right.mat', subj_list{ii})));
    
    fooof_results_vis.freq = fooof_results_vis.freq_range(1):fooof_results_vis.freq_range(2);
    fooof_results_vis.dimord = 'chan_freq';
    fooof_results_vis.label = {'EEG002'};
    fooof_results_vis.slope = fooof_results_vis.background_params(2);
    fooof_results_vis.intercept = fooof_results_vis.background_params(1);
    fooof_results_vis.fooofed_spectrum = 10 .^ fooof_results_vis.fooofed_spectrum; % reverse log-transform of fooof
    fooof_results_vis.flattened_spectrum = 10 .^ fooof_results_vis.flattened_spectrum; % reverse log-transform of fooof
    fooof_results_vis.original_spectrum = 10 .^ fooof_results_vis.original_spectrum; % reverse log-transform of fooof
    
    fooof_vis_right{ii} = fooof_results_vis;
    
    clear fooof_results*
    
end

%% do contrast

nfreqs = length(fooof_aud_left{1}.freq);

fooof_left_cntrst = cell(Nsub, 1);
fooof_right_cntrst = cell(Nsub, 1);
fooof_zeros_left = cell(Nsub, 1);
fooof_zeros_right = cell(Nsub, 1);

for ii = 1:Nsub
    
    fooof_left_cntrst{ii} = fooof_aud_left{ii};
    fooof_left_cntrst{ii}.original_spectrum = ((fooof_aud_left{ii}.original_spectrum - fooof_vis_left{ii}.original_spectrum) ./ (fooof_aud_left{ii}.original_spectrum + fooof_vis_left{ii}.original_spectrum)) .* 100;
    fooof_left_cntrst{ii}.flattened_spectrum = ((fooof_aud_left{ii}.flattened_spectrum - fooof_vis_left{ii}.flattened_spectrum) ./ (fooof_aud_left{ii}.flattened_spectrum + fooof_vis_left{ii}.flattened_spectrum)) .* 100;
    fooof_left_cntrst{ii}.fooofed_spectrum = ((fooof_aud_left{ii}.fooofed_spectrum - fooof_vis_left{ii}.fooofed_spectrum) ./ (fooof_aud_left{ii}.fooofed_spectrum + fooof_vis_left{ii}.fooofed_spectrum)) .* 100;
    
    fooof_right_cntrst{ii} = fooof_aud_right{ii};
    fooof_right_cntrst{ii}.original_spectrum = ((fooof_aud_right{ii}.original_spectrum - fooof_vis_right{ii}.original_spectrum) ./ (fooof_aud_right{ii}.original_spectrum + fooof_vis_right{ii}.original_spectrum)) .* 100;
    fooof_right_cntrst{ii}.flattened_spectrum = ((fooof_aud_right{ii}.flattened_spectrum - fooof_vis_right{ii}.flattened_spectrum) ./ (fooof_aud_right{ii}.flattened_spectrum + fooof_vis_right{ii}.flattened_spectrum)) .* 100;
    fooof_right_cntrst{ii}.fooofed_spectrum = ((fooof_aud_right{ii}.fooofed_spectrum - fooof_vis_right{ii}.fooofed_spectrum) ./ (fooof_aud_right{ii}.fooofed_spectrum + fooof_vis_right{ii}.fooofed_spectrum)) .* 100;
    
    fooof_zeros_left{ii} = fooof_aud_left{ii};
    fooof_zeros_left{ii}.original_spectrum = zeros(1, nfreqs);
    fooof_zeros_left{ii}.flattened_spectrum = zeros(1, nfreqs);
    fooof_zeros_left{ii}.fooofed_spectrum = zeros(1, nfreqs);
    
    fooof_zeros_right{ii} = fooof_aud_right{ii};
    fooof_zeros_right{ii}.original_spectrum = zeros(1, nfreqs);
    fooof_zeros_right{ii}.flattened_spectrum = zeros(1, nfreqs);
    fooof_zeros_right{ii}.fooofed_spectrum = zeros(1, nfreqs);
    
end

%% exclude subjects without peaks

exclude_aud_left = false(Nsub, 1);
exclude_vis_left = false(Nsub, 1);
exclude_aud_right = false(Nsub, 1);
exclude_vis_right = false(Nsub, 1);

for ii = 1:Nsub
    
    exclude_aud_left(ii) = ~isempty(fooof_aud_left{ii}.peaks_params);
    exclude_vis_left(ii) = ~isempty(fooof_vis_left{ii}.peaks_params);
    exclude_aud_right(ii) = ~isempty(fooof_aud_right{ii}.peaks_params);
    exclude_vis_right(ii) = ~isempty(fooof_vis_right{ii}.peaks_params);
    
end

exclude_both_left = logical(exclude_aud_left .* exclude_vis_left);
exclude_both_right = logical(exclude_aud_right .* exclude_vis_right);

%--------------------------------------------------------------------------
%%                descriptives for peak center frequencies
%--------------------------------------------------------------------------

aud_peak_freq_left = zeros(1, Nsub);
vis_peak_freq_left = zeros(1, Nsub);
aud_peak_freq_right = zeros(1, Nsub);
vis_peak_freq_right = zeros(1, Nsub);

for ii = 1:Nsub
    
    if exclude_aud_left(ii)
        aud_peak_freq_left(ii) = fooof_aud_left{ii}.peaks_params(1, 1);
    end
    
    if exclude_vis_left(ii)
        vis_peak_freq_left(ii) = fooof_vis_left{ii}.peaks_params(1, 1);
    end
    
    if exclude_aud_right(ii)
        aud_peak_freq_right(ii) = fooof_aud_right{ii}.peaks_params(1, 1);
    end
    
    if exclude_vis_right(ii)
        vis_peak_freq_right(ii) = fooof_vis_right{ii}.peaks_params(1, 1);
    end
    
end

% descriptives table

var_names = {'mean', 'std', 'min', 'max'};
var_types = {'double', 'double', 'double', 'double'};
n_vars = length(var_names);
row_names = {'left ear, auditory modality', 'left ear, visual modality', 'right ear, auditory modality', 'right ear, visual modality'};
descriptives_for_peak_params = table('Size', [4 n_vars], 'VariableTypes', var_types, 'VariableNames', var_names, 'RowNames', row_names);

% for left
descriptives_for_peak_params.mean(1) = mean(aud_peak_freq_left(exclude_both_left));
descriptives_for_peak_params.std(1) = std(aud_peak_freq_left(exclude_both_left));
descriptives_for_peak_params.mean(2) = mean(vis_peak_freq_left(exclude_both_left));
descriptives_for_peak_params.std(2) = std(vis_peak_freq_left(exclude_both_left));

descriptives_for_peak_params.max(1) = max(aud_peak_freq_left(exclude_both_left));
descriptives_for_peak_params.min(1) = min(aud_peak_freq_left(exclude_both_left));
descriptives_for_peak_params.max(2) = max(vis_peak_freq_left(exclude_both_left));
descriptives_for_peak_params.min(2) = min(vis_peak_freq_left(exclude_both_left));

% for right
descriptives_for_peak_params.mean(3) = mean(aud_peak_freq_right(exclude_both_right));
descriptives_for_peak_params.std(3) = std(aud_peak_freq_right(exclude_both_right));
descriptives_for_peak_params.mean(4) = mean(vis_peak_freq_right(exclude_both_right));
descriptives_for_peak_params.std(4) = std(vis_peak_freq_right(exclude_both_right));

descriptives_for_peak_params.max(3) = max(aud_peak_freq_right(exclude_both_right));
descriptives_for_peak_params.min(3) = min(aud_peak_freq_right(exclude_both_right));
descriptives_for_peak_params.max(4) = max(vis_peak_freq_right(exclude_both_right));
descriptives_for_peak_params.min(4) = min(vis_peak_freq_right(exclude_both_right));

disp('Descriptive statistics for OAA peak center frequencies:')
disp(descriptives_for_peak_params)

%--------------------------------------------------------------------------
%%             inferential stats for peak center frequencies
%--------------------------------------------------------------------------

[~, prob_peak_freq_left, ~, stat_peak_freq_left] = ttest(aud_peak_freq_left(exclude_both_left), vis_peak_freq_left(exclude_both_left));
[~, prob_peak_freq_right, ~, stat_peak_freq_right] = ttest(aud_peak_freq_right(exclude_both_right), vis_peak_freq_right(exclude_both_right));

% statistics table
var_names = {'T', 'df', 'p'};
var_types = {'double', 'double', 'double'};
n_vars = length(var_names);
row_names = {'left ear, auditory vs. visual', 'right ear, auditory vs. visual'};
statistics_for_peak_params = table('Size', [2 n_vars], 'VariableTypes', var_types, 'VariableNames', var_names, 'RowNames', row_names);

statistics_for_peak_params.T(1) = stat_peak_freq_left.tstat;
statistics_for_peak_params.df(1) = stat_peak_freq_left.df;
statistics_for_peak_params.p(1) = prob_peak_freq_left;

statistics_for_peak_params.T(2) = stat_peak_freq_right.tstat;
statistics_for_peak_params.df(2) = stat_peak_freq_right.df;
statistics_for_peak_params.p(2) = prob_peak_freq_right;

disp('Inferential statistics for OAA peak center frequencies:')
disp(statistics_for_peak_params)

%--------------------------------------------------------------------------
%%                descriptives for slopes
%--------------------------------------------------------------------------

aud_slopes_left = zeros(1, Nsub);
vis_slopes_left = zeros(1, Nsub);
aud_slopes_right = zeros(1, Nsub);
vis_slopes_right = zeros(1, Nsub);

for ii = 1:Nsub
    
    if exclude_aud_left(ii)
        aud_slopes_left(ii) = fooof_aud_left{ii}.slope(1, 1);
    end
    
    if exclude_vis_left(ii)
        vis_slopes_left(ii) = fooof_vis_left{ii}.slope(1, 1);
    end
    
    if exclude_aud_right(ii)
        aud_slopes_right(ii) = fooof_aud_right{ii}.slope(1, 1);
    end
    
    if exclude_vis_right(ii)
        vis_slopes_right(ii) = fooof_vis_right{ii}.slope(1, 1);
    end
    
end

% descriptives table

var_names = {'mean', 'std'};
var_types = {'double', 'double'};
n_vars = length(var_names);
row_names = {'left ear, auditory modality', 'left ear, visual modality', 'right ear, auditory modality', 'right ear, visual modality'};
descriptives_for_slopes = table('Size', [4 n_vars], 'VariableTypes', var_types, 'VariableNames', var_names, 'RowNames', row_names);

% for left
descriptives_for_slopes.mean(1) = mean(aud_slopes_left(exclude_both_left));
descriptives_for_slopes.std(1) = std(aud_slopes_left(exclude_both_left));
descriptives_for_slopes.mean(2) = mean(vis_slopes_left(exclude_both_left));
descriptives_for_slopes.std(2) = std(vis_slopes_left(exclude_both_left));

% for right
descriptives_for_slopes.mean(3) = mean(aud_slopes_right(exclude_both_right));
descriptives_for_slopes.std(3) = std(aud_slopes_right(exclude_both_right));
descriptives_for_slopes.mean(4) = mean(vis_slopes_right(exclude_both_right));
descriptives_for_slopes.std(4) = std(vis_slopes_right(exclude_both_right));

disp('Descriptive statistics for slopes of FOOOF models:')
disp(descriptives_for_slopes)

%--------------------------------------------------------------------------
%%             inferential stats for slopes
%--------------------------------------------------------------------------

%left
[~, prob_slope_left, ~, stat_slope_left] = ttest(aud_slopes_left, vis_slopes_left);

%right
[~, prob_slope_right, ~, stat_slope_right] = ttest(aud_slopes_right, vis_slopes_right);

% statistics table
var_names = {'T', 'df', 'p'};
var_types = {'double', 'double', 'double'};
n_vars = length(var_names);
row_names = {'left ear, auditory vs. visual', 'right ear, auditory vs. visual'};
statistics_for_slopes = table('Size', [2 n_vars], 'VariableTypes', var_types, 'VariableNames', var_names, 'RowNames', row_names);

statistics_for_slopes.T(1) = stat_slope_left.tstat;
statistics_for_slopes.df(1) = stat_slope_left.df;
statistics_for_slopes.p(1) = prob_slope_left;

statistics_for_slopes.T(2) = stat_slope_right.tstat;
statistics_for_slopes.df(2) = stat_slope_right.df;
statistics_for_slopes.p(2) = prob_slope_right;

disp('Inferential statistics for slopes of FOOOF models:')
disp(statistics_for_slopes)

%--------------------------------------------------------------------------
%%            descriptives for left & right contrast
%--------------------------------------------------------------------------

% get descriptives for cntrst

sel_fooof_left_cntrst = cell(Nsub, 1);
sel_fooof_right_cntrst = cell(Nsub, 1);

for ii = 1:Nsub
    
    cfg = [];
    cfg.frequency = [3 10]; % average over frequency range from OAA descriptives
    cfg.avgoverfreq = 'yes';
    sel_fooof_left_cntrst{ii} = ft_selectdata(cfg, fooof_left_cntrst{ii});
    
    cfg.frequency = [1 10]; % average over frequency range from OAA descriptives
    sel_fooof_right_cntrst{ii} = ft_selectdata(cfg, fooof_right_cntrst{ii});
    sel_fooof_right_cntrst{ii}.label{1} = 'EEG001';
    sel_fooof_right_cntrst{ii}.freq = sel_fooof_left_cntrst{ii}.freq;
    
end

cfg = [];
cfg.parameter = 'fooofed_spectrum';
cfg.keepindividual = 'yes';
ga_fooof_left_cntrst = ft_freqgrandaverage(cfg, sel_fooof_left_cntrst{:});
ga_fooof_right_cntrst = ft_freqgrandaverage(cfg, sel_fooof_right_cntrst{:});

% descritpives table
var_names = {'mean', 'std'};
var_types = {'double', 'double'};
n_vars = length(var_names);
row_names = {'left ear', 'right ear'};
descriptives_for_cntrst = table('Size', [2 n_vars], 'VariableTypes', var_types, 'VariableNames', var_names, 'RowNames', row_names);

descriptives_for_cntrst.mean(1) = mean(ga_fooof_left_cntrst.fooofed_spectrum);
descriptives_for_cntrst.std(1) = std(ga_fooof_left_cntrst.fooofed_spectrum);
descriptives_for_cntrst.mean(2) = mean(ga_fooof_right_cntrst.fooofed_spectrum);
descriptives_for_cntrst.std(2) = std(ga_fooof_right_cntrst.fooofed_spectrum);

disp('Descriptive statistics for OAA contrast:')
disp(descriptives_for_cntrst)

%--------------------------------------------------------------------------
%%            inferential stats for left & right contrast
%--------------------------------------------------------------------------

% left
cfg = [];
cfg.parameter = 'fooofed_spectrum';
cfg.avgoverfreq = 'yes';
cfg.frequency = [3 10];
cfg.method = 'analytic';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.alpha = 0.05;
cfg.tail = 1;
 
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

stat_left = ft_freqstatistics(cfg, fooof_left_cntrst{:}, fooof_zeros_left{:});

% right
cfg = [];
cfg.parameter = 'fooofed_spectrum';
cfg.avgoverfreq = 'yes';
cfg.frequency = [1 10];
cfg.method = 'analytic';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.alpha = 0.05;
cfg.tail = 1;
 
cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

stat_right = ft_freqstatistics(cfg, fooof_right_cntrst{:}, fooof_zeros_right{:});

% compare left and right effect
cfg = [];
cfg.parameter = 'fooofed_spectrum';
cfg.avgoverchan = 'yes';
cfg.method = 'analytic';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.alpha = 0.05;

cfg.design(1,1:2*Nsub)  = [ones(1,Nsub) 2*ones(1,Nsub)];
cfg.design(2,1:2*Nsub)  = [1:Nsub 1:Nsub];
cfg.ivar                = 1; % the 1st row in cfg.design contains the independent variable
cfg.uvar                = 2; % the 2nd row in cfg.design contains the subject number

stat_left_right = ft_freqstatistics(cfg, sel_fooof_left_cntrst{:}, sel_fooof_right_cntrst{:});

% statistics table
var_names = {'T', 'df', 'p'};
var_types = {'double', 'double', 'double'};
n_vars = length(var_names);
row_names = {'left ear', 'right ear', 'left vs. right ear'};
statistics_for_contrast = table('Size', [3 n_vars], 'VariableTypes', var_types, 'VariableNames', var_names, 'RowNames', row_names);

statistics_for_contrast.T(1) = stat_left.stat;
statistics_for_contrast.df(1) = stat_left.df;
statistics_for_contrast.p(1) = stat_left.prob;

statistics_for_contrast.T(2) = stat_right.stat;
statistics_for_contrast.df(2) = stat_right.df;
statistics_for_contrast.p(2) = stat_right.prob;

statistics_for_contrast.T(3) = stat_left_right.stat;
statistics_for_contrast.df(3) = stat_left_right.df;
statistics_for_contrast.p(3) = stat_left_right.prob;

disp('Inferential statistics for OAA contrast:')
disp(statistics_for_contrast)