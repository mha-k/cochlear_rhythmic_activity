%% init

str = [];

while isempty(str)
prompt = 'Please type or copy the path to your obob_ownft:';
str = input(prompt, 's');
end

addpath(str);
cfg = [];
obob_init_ft(cfg);

% define which files you like to load
min_freq = 500; % define the minimum freq you want to plot
max_freq = 2500; % define the maximum freq you want to plot
freq_steps = 10; % change the number of hilbert windows
hilbert_width = 30; % define the width of the hilbert window in one direction (full window = hilbert_width * 2)
fft_min_freq = 1; % choose the minimum freq for the MTMFFT
fft_max_freq = 80; % choose the maximum freq for the MTMFFT

% put parameters in strings
str_min_freq = num2str(min_freq);
str_max_freq = num2str(max_freq);
str_freq_steps = num2str(freq_steps);
str_hilbert_width = num2str(hilbert_width);
str_fft_min_freq = num2str(fft_min_freq);
str_fft_max_freq = num2str(fft_max_freq);
str_parameter = [str_fft_min_freq '_' str_fft_max_freq '_' str_min_freq '_' str_max_freq '_' str_freq_steps '_' str_hilbert_width];

if ispc
    
    in_dir_OAE_multiple_hilbert_fft = '.\data/oaa_power_spectra\';
    addpath('.\utility_functions\');
    
else
    
    in_dir_OAE_multiple_hilbert_fft = '.\data/oaa_power_spectra\';
    addpath('.\utility_functions\');
    
end

%% get subject list

subj_list = {
  '19890720ADLH';...
  '19950717ADTO';...
  '19960606MRRI';...
%   '19960817RGOE';... % excluded
  '19980530IIBR';...
  '19910823SSLD';...
  '19930506URHE';...
  '19980105PTLN';...
%   '19890227VRBA';... % excluded
  '19940613BTRI';... % #10
  '19920805CRLD';... 
  '19820101CRVL';...
  '19951106DRML';... 
  '19970218CRPO';... 
  '19990531IASH';...
  '19951209DRDN';...
  '19970714MRZN';...
  '19980704FBMF';...
  '19961106SBGI';...
%   '19950114URMR';... % #20 excluded
  '19970209SBRD';...
%   '19930630MNSU';... % excluded
%   '19900106DGBN';... % excluded
%   '19890714GBMN';... % excluded
%   '19962223CAHN';... % excluded
  '19891222GBHL';...
  '19910612CRKE';... 
  '19880705LHKO';...
  '19930630MNSU';...
  '19970421MRKL';... % #30
  '19990328SGRI';... 
  '19950508EISH';...
  '19960407MNSI';...
  '20010827MNMC';... % #34
  };

%% load all subjects

% preallocate size of cell arrays
n_hilbert_windows = (max_freq - min_freq) / freq_steps +1;
psd_all_subj_aud = cell(length(subj_list), n_hilbert_windows);
psd_all_subj_vis = cell(length(subj_list), n_hilbert_windows);

for ii = 1:length(subj_list) % load all subjects and save in 1 cell array
    
        load(fullfile(in_dir_OAE_multiple_hilbert_fft, sprintf('%s_OAE_multiple_hilbert_fft_short_%s.mat', subj_list{ii}, str_parameter)), 'psd*');
    
    for jj = 1:length(psd_hilbert_aud_all)
        
        psd_all_subj_aud{ii, jj} = psd_hilbert_aud_all{1, jj};
        psd_all_subj_vis{ii, jj} = psd_hilbert_vis_all{1, jj};
        
    end
    
    clear psd_hilbert_*;
    
end

%% prepare data and smooth

% smoothing parameters
Nr = 2; % smoothing factor for rows
Nc = 5; % smoothing factor for columns
str_Nr = num2str(Nr);
str_Nc = num2str(Nc);

% prepare structure to fool fieldtrip and do a TFR-Plot with the hilbert windows as time-axis
smoothed_psd_all_subj_aud = cell(length(subj_list), 1); % put in a separate struct
smoothed_psd_all_subj_vis = smoothed_psd_all_subj_aud; % use the struct for aud also for vis
smoothed_left_psd_all_subj_aud = smoothed_psd_all_subj_aud;
smoothed_left_psd_all_subj_vis = smoothed_psd_all_subj_aud;
smoothed_right_psd_all_subj_aud = smoothed_psd_all_subj_aud;
smoothed_right_psd_all_subj_vis = smoothed_psd_all_subj_aud;

for ii = 1:length(subj_list)
  
  % fake the TFR-structure
  smoothed_psd_all_subj_aud{ii,1}.dimord = 'chan_freq_time'; % change dimord to get a time axis
  smoothed_psd_all_subj_aud{ii,1}.time = min_freq:freq_steps:max_freq; % set time axis using the range and steps of the hilbert windows
  smoothed_psd_all_subj_aud{ii,1}.freq = fft_min_freq:1:fft_max_freq; % replace the unevenly distributed freqs so fieldtrip doesn't complain
  smoothed_psd_all_subj_aud{ii,1}.powspctrm = zeros(2, (fft_max_freq - fft_min_freq) +1, length(psd_all_subj_aud)); % preallocate a 3-D matrix for powspctrm
  smoothed_psd_all_subj_aud{ii,1}.label = psd_all_subj_aud{1, 1}.label;
  
  smoothed_psd_all_subj_vis{ii,1}.dimord = 'chan_freq_time'; % change dimord to get a time axis
  smoothed_psd_all_subj_vis{ii,1}.time = min_freq:freq_steps:max_freq; % set time axis using the range and steps of the hilbert windows
  smoothed_psd_all_subj_vis{ii,1}.freq = fft_min_freq:1:fft_max_freq; % replace the unevenly distributed freqs so fieldtrip doesn't complain
  smoothed_psd_all_subj_vis{ii,1}.powspctrm = zeros(2, (fft_max_freq - fft_min_freq) +1, length(psd_all_subj_aud)); % preallocate a 3-D matrix for powspctrm
  smoothed_psd_all_subj_vis{ii,1}.label = psd_all_subj_aud{1, 1}.label;
  
  for jj = 1:length(psd_all_subj_aud)
    
    smoothed_psd_all_subj_aud{ii,1}.powspctrm(:, :, jj) = psd_all_subj_aud{ii, jj}.powspctrm;
    smoothed_psd_all_subj_vis{ii,1}.powspctrm(:, :, jj) = psd_all_subj_vis{ii, jj}.powspctrm;
    
  end
  
  % do the smoothing
  cfg = [];
  cfg.channel = 'EEG001';
  smoothed_left_psd_all_subj_aud{ii,1} = ft_selectdata(cfg, smoothed_psd_all_subj_aud{ii,1});
  smoothed_left_psd_all_subj_vis{ii,1} = ft_selectdata(cfg, smoothed_psd_all_subj_vis{ii,1});
  
  smoothed_left_psd_all_subj_aud{ii,1}.powspctrm = squeeze(smoothed_left_psd_all_subj_aud{ii,1}.powspctrm);
  smoothed_left_psd_all_subj_vis{ii,1}.powspctrm = squeeze(smoothed_left_psd_all_subj_vis{ii,1}.powspctrm);
  
  smoothed_left_psd_all_subj_aud{ii,1}.powspctrm = Smooth2(smoothed_left_psd_all_subj_aud{ii,1}.powspctrm, 'GaussConv', [Nr, Nc]);
  smoothed_left_psd_all_subj_vis{ii,1}.powspctrm = Smooth2(smoothed_left_psd_all_subj_vis{ii,1}.powspctrm, 'GaussConv', [Nr, Nc]);
  
  cfg = [];
  cfg.channel = 'EEG002';
  smoothed_right_psd_all_subj_aud{ii,1} = ft_selectdata(cfg, smoothed_psd_all_subj_aud{ii,1});
  smoothed_right_psd_all_subj_vis{ii,1} = ft_selectdata(cfg, smoothed_psd_all_subj_vis{ii,1});
  
  smoothed_right_psd_all_subj_aud{ii,1}.powspctrm = squeeze(smoothed_right_psd_all_subj_aud{ii,1}.powspctrm);
  smoothed_right_psd_all_subj_vis{ii,1}.powspctrm = squeeze(smoothed_right_psd_all_subj_vis{ii,1}.powspctrm);
  
  smoothed_right_psd_all_subj_aud{ii,1}.powspctrm = Smooth2(smoothed_right_psd_all_subj_aud{ii,1}.powspctrm, 'GaussConv', [Nr, Nc]);
  smoothed_right_psd_all_subj_vis{ii,1}.powspctrm = Smooth2(smoothed_right_psd_all_subj_vis{ii,1}.powspctrm, 'GaussConv', [Nr, Nc]);
  
  smoothed_psd_all_subj_aud{ii,1}.powspctrm(1, :, :) = smoothed_left_psd_all_subj_aud{ii, 1}.powspctrm;
  smoothed_psd_all_subj_vis{ii,1}.powspctrm(1, :, :) = smoothed_left_psd_all_subj_vis{ii, 1}.powspctrm;
  smoothed_psd_all_subj_aud{ii,1}.powspctrm(2, :, :) = smoothed_right_psd_all_subj_aud{ii, 1}.powspctrm;
  smoothed_psd_all_subj_vis{ii,1}.powspctrm(2, :, :) = smoothed_right_psd_all_subj_vis{ii, 1}.powspctrm;
  
end

clear smoothed_left_psd_all_subj_*; clear smoothed_right_psd_all_subj_*;

%% do contrast

psd_all_subj_smoothed_cntrst = smoothed_psd_all_subj_aud;

for ii = 1:length(subj_list)
  
  for jj = 1:n_hilbert_windows
    
    psd_all_subj_smoothed_cntrst{ii, 1}.powspctrm(1, :, jj) = ((smoothed_psd_all_subj_aud{ii, 1}.powspctrm(1, :, jj) - smoothed_psd_all_subj_vis{ii, 1}.powspctrm(1, :, jj)) ./ (smoothed_psd_all_subj_aud{ii, 1}.powspctrm(1, :, jj) + smoothed_psd_all_subj_vis{ii, 1}.powspctrm(1, :, jj))) .* 100;
    psd_all_subj_smoothed_cntrst{ii, 1}.powspctrm(2, :, jj) = ((smoothed_psd_all_subj_aud{ii, 1}.powspctrm(2, :, jj) - smoothed_psd_all_subj_vis{ii, 1}.powspctrm(2, :, jj)) ./ (smoothed_psd_all_subj_aud{ii, 1}.powspctrm(2, :, jj) + smoothed_psd_all_subj_vis{ii, 1}.powspctrm(2, :, jj))) .* 100;
    
  end
  
end

%% do grand average

cfg = [];
ga_cntrst = ft_freqgrandaverage(cfg, psd_all_subj_smoothed_cntrst{:});

%% left & right anti-alias

cfg = [];
cfg.latency = [1000 2000];
cfg.frequency = [1 45];
ga_cntrst = ft_selectdata(cfg, ga_cntrst);

meanpow_left = squeeze(ga_cntrst.powspctrm(1, :, :));
meanpow_right = squeeze(ga_cntrst.powspctrm(2, :, :));

tim_interp = linspace(1000, 2000, 512);
freq_interp = linspace(1, 45, 228);

[tim_grid_orig, freq_grid_orig] = meshgrid(ga_cntrst.time, ga_cntrst.freq);
[tim_grid_interp, freq_grid_interp] = meshgrid(tim_interp, freq_interp);

pow_interp_left = interp2(tim_grid_orig, freq_grid_orig, meanpow_left, tim_grid_interp, freq_grid_interp, 'spline');
pow_interp_right = interp2(tim_grid_orig, freq_grid_orig, meanpow_right, tim_grid_interp, freq_grid_interp, 'spline');

pow_at_toi_left = mean(pow_interp_left, 2); % pow_interp(:,timind);
pow_at_toi_right = mean(pow_interp_right, 2); % pow_interp(:,timind);

max_power_left = round(max(max(pow_interp_left, [], 1)), 1);
max_power_right = round(max(max(pow_interp_right, [], 1)), 1);

if max_power_left > max_power_right
  
  max_power = max_power_left;
  
else
  
  max_power = max_power_right;
  
end

%% left plot

format long

figure();
ax_main = axes('Position', [0.1 0.2 0.55 0.55]);
ax_right = axes('Position', [0.7 0.2 0.1 0.55]);

axes(ax_main);
im_main = imagesc(tim_interp, freq_interp, pow_interp_left);
xlim([1000 2000]);
ylim([1 30]);
pbaspect([0.8 1 1])
axis xy;
title('AMI of Left Ear')
xlabel('Cochlear Frequency Response (Hz)')
ylabel('Frequency (Hz)')

CT = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
colormap(CT);
h = colorbar();
h.Limits = [-max_power max_power];
h.Ticks = [-max_power max_power];
h.TickLength = 0;
h.Position = [0.85 0.2 0.03 0.55];
h.FontName = 'Helvetica-Normal';
h.FontSize = 10;
h.Label.String = 'AMI (%)';
h.Label.FontSize = 10;
h.Label.FontName = 'Helvetica-Normal';

ax = gca(); % this Gets the Current Axis so we can set properties
ax.TickDir = 'out';
ax.XTick = [1000 1500 2000];
ax.YTick = [1 10 20 30];
ax.LineWidth = 1;
ax.FontName = 'Helvetica-Normal';
ax.FontSize = 10;

box off;

axes(ax_right);
area(freq_interp, pow_at_toi_left,...
    'EdgeColor', 'none', 'FaceColor', [0.5 0.5 0.5]);
view([270 90]); % this rotates the plot
ax_right.YDir = 'reverse';
box off;
ax_right.XTickLabel = [];
ylab = ylabel({'AMI'; '(%)'});
xlim([1 30]);

ax = gca(); % this Gets the Current Axis so we can set properties
ax.TickDir = 'out';
ax.XTick = [10 20 30]; % [10 20 30];
ax.YTick = [0 1];
ylim([0 max(pow_at_toi_left)]);
% ax.XColor = [1 0 0];
ax.LineWidth = 1;
ax.FontName = 'Helvetica-Normal';
ax.FontSize = 10;

%% right plot

format long

figure();
ax_main = axes('Position', [0.1 0.2 0.55 0.55]);
ax_right = axes('Position', [0.7 0.2 0.1 0.55]);

axes(ax_main);
im_main = imagesc(tim_interp, freq_interp, pow_interp_right);
xlim([1000 2000]);
ylim([1 30]);
pbaspect([0.8 1 1])
axis xy;
title('AMI of Right Ear')
xlabel('Cochlear Frequency Response (Hz)')
ylabel('Frequency (Hz)')

CT = flipud(cbrewer('div', 'RdYlBu', 256, 'PCHIP'));
colormap(CT);
h = colorbar();
h.Limits = [-max_power max_power];
h.Ticks = [-max_power max_power];
h.TickLength = 0;
h.Position = [0.85 0.2 0.03 0.55];
h.FontName = 'Helvetica-Normal';
h.FontSize = 10;
h.Label.String = 'AMI (%)';
h.Label.FontSize = 10;
h.Label.FontName = 'Helvetica-Normal';

ax = gca(); % this Gets the Current Axis so we can set properties
ax.TickDir = 'out';
ax.XTick = [1000 1500 2000];
ax.YTick = [1 10 20 30];
ax.LineWidth = 1;
ax.FontName = 'Helvetica-Normal';
ax.FontSize = 10;

box off;

axes(ax_right);
area(freq_interp, pow_at_toi_right,...
    'EdgeColor', 'none', 'FaceColor', [0.5 0.5 0.5]);
view([270 90]); % this rotates the plot
ax_right.YDir = 'reverse';
box off;
ax_right.XTickLabel = [];
ylab = ylabel({'AMI'; '(%)'});
xlim([1 30]);

ax = gca(); % this Gets the Current Axis so we can set properties
ax.TickDir = 'out';
ax.XTick = [10 20 30]; % [10 20 30];
ax.YTick = [0 1];
ylim([min(pow_at_toi_right) max(pow_at_toi_right)]);
% ax.XColor = [1 0 0];
ax.LineWidth = 1;
ax.FontName = 'Helvetica-Normal';
ax.FontSize = 10;