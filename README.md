## cochlear_rhythmic_activity
 
This repository contains code to reproduce the statistics and figures from Köhler, M. H. A., Demarchi, G., & Weisz, N. (2020). Cochlear activity in silent cue-target intervals shows a theta-rhythmic pattern and is correlated to attentional alpha and theta modulations. bioRxiv, 653311. https://doi.org/10.1101/653311
 
## Requirements

1. R (preferably >= 4.0.0)
2. RStudio
3. Matlab >= 2018a
4. obob_ownft (The Salzburg Brain Dynamics Lab's M/EEG analysis environment) You can find it here: https://gitlab.com/obob/obob_ownft

## Instructions

The repository contains two folders. "matlab" and "R" contain all scripts, functions, and data to reproduce the statistics and figures made by them. For "matlab" simply enter the folder and run the scripts. For "R" open the R-project-file and run the scripts.
